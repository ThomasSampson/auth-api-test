/**
 * @file auth.test.js
 * Contains unit tests for the Authentication API
 * 
 */
'use strict'

process.env['Auth0Domain'] = "https://test.mnb.eu.auth0.com";
process.env['Auth0Api'] = "/oauth/token";
process.env['Auth0ClientId'] = "clientId";
process.env['Auth0ClientSecret'] = "client_secret";

var assert = require ('assert');

var authenticationTest = require ( './auth' );

var nock = require ('nock');

describe ( 'authenticationTest', function () {
    [
        {
            "username" : "validUser@decisioninsightgroup.co.uk",
            "password" : "test"
        }
        
    ].forEach ( function ( validUserNamePassword ) {

        nock ( process.env.Auth0Domain )        
        .post ( process.env.Auth0Api, {
            'grant_type': 'password',
            'username': 'validUser@decisioninsightgroup.co.uk',
            'password': 'test',
            'client_id': process.env.Auth0ClientId,
            'client_secret' : process.env.Auth0ClientSecret
        })
        .reply (200, JSON.stringify ( {
            'isAuthenticated': true,
            'access_token': 'access_token-1234567',
            'expires_in': 86400,
            'scope': 'openid profile email address phone',
            'id_token': 'id token from auth0',
            'token_type': 'Bearer',           
            'error_description': null
        } ) );

        it ( 'successful invocation: [username=' + validUserNamePassword.username + 
            ', password: ' + validUserNamePassword.password + ']', function ( done ) {
         
                var context = {
                    requestId : 'd9da1fbc-3bcb-11e7-884e-81e7570060d8',
                    succeed: function ( result ) {                        
                    },

                fail: function ( result ) {                        
                    }
                }
                authenticationTest.handler ( validUserNamePassword, context, 
                    function (error, result ) {                      
                        assert.notEqual (result, null, 'Expected a result here');
                        assert.equal (result.isAuthenticated, true, 'isAuthenticated should be set to true');
                        assert.equal (result.access_token, 'access_token-1234567', 'Result should have a access tocken');
                        assert.equal (result.id_token, 'id token from auth0', 'response should have an id_token in it');
                        assert.equal (result.token_type, 'Bearer', 'Token type expected is Bearer');
                        assert.notEqual (result.expires_in, null, 'There should be a tocken expiration');
                        assert.equal (result.error_description, null);

                        done ();                           
                });
        });
    });
});


describe ( 'authenticationFailTest', function () {
    [
        {
            "username" : "InvaldiUser",
            "password" : "Invalid"
        }
    ].forEach ( function ( invalidUsernameAndPassword ) {

         nock ( process.env.Auth0Domain )                
        .post ( process.env.Auth0Api, {
            'grant_type': 'password',
            'username': 'InvaldiUser',
            'password': 'Invalid',
            'client_id': process.env.Auth0ClientId,
            'client_secret' : process.env.Auth0ClientSecret
        } )            
        .reply (403, JSON.stringify ({
            'isAuthenticated': false,
            'errorType': 'Forbidden',
            'requestId': 'd9da1fbc-3bcb-11e7-884e-81e7570060d8',
            'httpStatus': 403,
            'message': 'Invalid login credentials. Username or password is not matching'           
            
        }) );

        it ( 'fail: when name is invalid: [username=' + invalidUsernameAndPassword.username + 
            ', password: ' + invalidUsernameAndPassword.password + ']', function ( done ) {

            var context = {
                requestId : 'd9da1fbc-3bcb-11e7-884e-81e7570060d8',
                succeed: function ( result ) {                                                                     
                    },

                fail: function () {                        
                }            
                
                }
                authenticationTest.handler ( invalidUsernameAndPassword, context, 
                    function ( error ) {                        
                        var errorResponse = JSON.parse(error);   
                       
                        assert.equal (errorResponse.isAuthenticated, false, 'isAuthenticated should be false');
                        assert.equal (errorResponse.access_token, null, 'Access token should be null');
                        assert.equal (errorResponse.id_token, null, 'response should have an id_token in it');
                        assert.equal (errorResponse.token_type, null, 'Token type expected is Bearer');
                        assert.equal (errorResponse.expires_in, null, 'There should be a tocken expiration');
                        //assert.equal (errorResponse.errorType, "Forbidden", "Error type should be 'Forbidden' for wrong username and password");
                        assert.equal (errorResponse.httpStatus, 403, 'https status expected is 403');
                       //assert.notEqual(errorResponse.message, null, 'A message is expected');      
                        assert.notEqual(errorResponse.requestId, null, 'Response should have a request id');
                        
                        done ();
                });
        });
    });

});