/**
 * @file auth.js
 * Provides API for authenticating user using Auth0 API
 * Environment variables required for this function
 *  Auth0Domain       - This should be set to auth0 domain. 
 *                      Example = https://test.mnb.eu.auth0.com
 *  Auth0Api          - API for authentication token. Should be '/oauth/token' 
 *                      Make sure the API begin with '/'
 *  Auth0ClientId     - Client id from Auth0 account
 *  Auth0ClientSecret - Client secret from Auth0 secret
 */

'use strict'

var request = require ( 'request' );
// Authenticate user with credentials available in event object.
// Result is send back using context reference.

exports.handler = (event, context, callback) => {
    console.log( 'Authenticating user '+ event.username);

    var options = buildAuthRequest ( event.username, event.password ); 
    
      request( options, function responseCallback ( error, response, body ) {
         var isAuthenticated = (error == null && response.statusCode == 200);        
            
        if ( isAuthenticated ) {            
            var authResult = authSuccessResult( response.body );                                
            callback(null, authResult);
         }
         else {                          
             console.log( 'authenticated failed :(');
             var authResult = authFailureResult( body.error, response.statusCode, context.requestId, body.error_description);            
             callback(JSON.stringify(authResult));           
         }        
    }); 
};

// Builds the request for Auth0 API
var buildAuthRequest = function ( username, userPassword ) {    
    var options = {
        method: 'POST',
        url:  process.env.Auth0Domain +  process.env.Auth0Api,
        headers: { 'Content-Type' : 'application/json' },
        body: {
            'grant_type': 'password',
            'username': username,
            'password': userPassword,
            'client_id': process.env.Auth0ClientId,
            'client_secret' : process.env.Auth0ClientSecret
        },
        json: true        
    };   
    return options;
}

//Builds authentication success result object 
var authSuccessResult = function ( responseBody ) {    
    var result = {
        'isAuthenticated' : true,
        'access_token' : responseBody.access_token,
        'expires_in' : responseBody.expires_in,
        'scope': responseBody.scope,
        'id_token' : responseBody.id_token,
        'token_type' : responseBody.token_type
    }    
    return result;
}

//Builds authentication failure result object 
var authFailureResult = function ( errType, httpStatusCode, ctxRequestId, userMessage ) {    
    var result = {
        errorType : errType,        
        httpStatus : httpStatusCode,
        requestId : ctxRequestId,
        message: userMessage,
        isAuthenticated : false      
        
    }
    return result;
}
